package cn.mrx.eas;

import lombok.extern.slf4j.Slf4j;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Author: Mr.X
 * Date: 2017/12/6 下午2:08
 * Description:
 */
@Slf4j
public class LogbackDemo {
    private Logger logger = LoggerFactory.getLogger(LogbackDemo.class);

    @Test
    public void test01(){
        logger.error("111111");
    }

    @Test
    public void test02(){
        log.error("222222");
    }
}
