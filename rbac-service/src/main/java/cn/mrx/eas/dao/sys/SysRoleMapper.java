package cn.mrx.eas.dao.sys;

import cn.mrx.eas.dto.sys.SysRole;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface SysRoleMapper {

    List<SysRole> roleListPage();

    SysRole findOneById(Integer id);

    int editRole(SysRole sysRole);

    int addRole(SysRole sysRole);

    int batchDel(String[] idArr);

    List<SysRole> findAll();

    List<SysRole> findAllByUserId(Integer userId);
}