package cn.mrx.eas.controller;

import cn.mrx.eas.dto.sys.SysRole;
import cn.mrx.eas.dto.sys.SysUser;
import cn.mrx.eas.server.BSGrid;
import cn.mrx.eas.server.ServerResponse;
import cn.mrx.eas.service.sys.ISysRoleService;
import cn.mrx.eas.service.sys.ISysUserService;
import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authz.UnauthorizedException;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Author: Mr.X
 * Date: 2017/12/23 下午5:57
 * Description:
 */
@Controller
@RequestMapping("/sys/user")
public class SysUserController {

    @Autowired
    private ISysUserService iSysUserService;

    @Autowired
    private ISysRoleService iSysRoleService;

    // 用户列表页面
    @RequiresPermissions("sys:user:list")
    @GetMapping
    public String index() {
        return "sys/user";
    }

    // 分页查询用户逻辑
    @RequiresPermissions("sys:user:list")
    @PostMapping("/list")
    @ResponseBody
    public BSGrid list(Integer curPage, Integer pageSize) {
        return iSysUserService.userListPage(curPage, pageSize);
    }

    // 编辑用户页面
    @RequiresPermissions("sys:user:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Integer id, Model model) {
        SysUser sysUser = iSysUserService.findOneById(id);
        List<SysRole> sysRoleList = iSysRoleService.findAllByUserId(id);
        model.addAttribute("sysUser", sysUser);
        model.addAttribute("sysRoleList", sysRoleList);
        return "sys/user-edit";
    }

    // 编辑用户逻辑
    @RequiresPermissions("sys:user:edit")
    @PostMapping("/edit")
    @ResponseBody
    public ServerResponse edit(SysUser sysUser, @RequestParam(value = "roleIds[]") Integer[] roleIds) {
        return iSysUserService.editUser(sysUser, roleIds);
    }

    // 添加用户页面
    @RequiresPermissions("sys:user:add")
    @GetMapping("/add")
    public String add(Model model) {
        List<SysRole> sysRoleList = iSysRoleService.findAllByUserId(null);
        model.addAttribute("sysRoleList", sysRoleList);
        return "sys/user-add";
    }

    // 添加用户逻辑
    @RequiresPermissions("sys:user:add")
    @PostMapping("/add")
    @ResponseBody
    public ServerResponse add(SysUser sysUser, @RequestParam(value = "roleIds[]") Integer[] roleIds) {
        return iSysUserService.addUser(sysUser, roleIds);
    }

    // 批量删除用户逻辑
    @RequiresPermissions("sys:user:batchDel")
    @PostMapping("/batchDel")
    @ResponseBody
    public ServerResponse batchDel(String ids) {
        return iSysUserService.batchDel(ids);
    }

    // 个人信息页面
    //@RequiresPermissions("sys:user:pi")
    @GetMapping("/pi/{id}")
    public String pi(@PathVariable("id") Integer id, Model model) {
        SysUser sessionSysUser = (SysUser) SecurityUtils.getSubject().getPrincipal();
        if (sessionSysUser.getId() != id) {
            throw new UnauthorizedException("禁止横向越权操作");
        }
        SysUser sysUser = iSysUserService.findOneById(id);
        List<SysRole> sysRoleList = iSysRoleService.findAllByUserId(id);
        model.addAttribute("sysUser", sysUser);
        model.addAttribute("sysRoleList", sysRoleList);
        return "sys/user-pi";
    }
}